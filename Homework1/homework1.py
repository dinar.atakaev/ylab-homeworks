# Задача №1
def domain_name(url):
    return url.split("www.")[-1].split("//")[-1].split(".")[0]

assert domain_name("http://google.com") == "google"
assert domain_name("http://google.co.jp") == "google"
assert domain_name("www.xakep.ru") == "xakep"
assert domain_name("https://youtube.com") == "youtube"


# Задача №2
def int32_to_ip(int32):
    a = int(int32 / 16777216) % 256
    b = int(int32 / 65536) % 256
    c = int(int32 / 256) % 256
    d = int(int32) % 256
    return '%(a)s.%(b)s.%(c)s.%(d)s' % locals()

assert int32_to_ip(2154959208) == "128.114.17.104"
assert int32_to_ip(0) == "0.0.0.0"
assert int32_to_ip(2149583361) == "128.32.10.1"


# Задача №3
def zeros(n):
    x = n // 5
    y = x
    while x > 0:
        x /= 5
        y += int(x)
    return y

assert zeros(0) == 0
assert zeros(6) == 1
assert zeros(30) == 7





# Задача №4
import itertools


def bananas(s):
    result = set()

    for comb in itertools.combinations(range(len(s)), len(s) - 6):
        arr = list(s)

        for i in comb:
            arr[i] = '-'

        candidate = ''.join(arr)

        if candidate.replace('-', '') == 'banana':
            result.add(candidate)

    return result


assert bananas("banann") == set()
assert bananas("banana") == {"banana"}
assert bananas("bbananana") == {"b-an--ana", "-banana--", "-b--anana", "b-a--nana", "-banan--a",
                     "b-ana--na", "b---anana", "-bana--na", "-ba--nana", "b-anan--a",
                     "-ban--ana", "b-anana--"}
assert bananas("bananaaa") == {"banan-a-", "banana--", "banan--a"}
assert bananas("bananana") == {"ban--ana", "ba--nana", "bana--na", "b--anana", "banana--", "banan--a"}



# Задача №5
def count_find_num(primesL, limit):
    primesL = sorted(primesL)
    count = 1
    for val in primesL:
        count *= val
    if count > limit:
        return []
    final_value = [count]
    for i in primesL:
        for val in final_value:
            num = val * i
            if (num <= limit) and (num not in final_value):
                final_value.append(num)
                num *= i

    return [len(final_value), max(final_value)]



primesL = [2, 3]
limit = 200
assert count_find_num(primesL, limit) == [13, 192]

primesL = [2, 5]
limit = 200
assert count_find_num(primesL, limit) == [8, 200]

primesL = [2, 3, 5]
limit = 500
assert count_find_num(primesL, limit) == [12, 480]

primesL = [2, 3, 5]
limit = 1000
assert count_find_num(primesL, limit) == [19, 960]

primesL = [2, 3, 47]
limit = 200
assert count_find_num(primesL, limit) == []
