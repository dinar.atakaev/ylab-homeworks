from math import pi
from math import tan
from typing import Tuple
from abc import ABCMeta, abstractmethod

class Shape(metaclass=ABCMeta):
    title: str = 'Фигура'

    @abstractmethod
    def area(self) -> float:
        pass


class Square(Shape):
    title: str = 'Квадрат'

    def __init__(self, a: float) -> None:
        self.a = a

    def area(self) -> float:
        return round((self.a ** 2), 2)

    @staticmethod
    def diagonal(a: float) -> float:
        return round((a * (2 ** (1 / 2))), 2)


class Rectangle(Shape):
    title: str = 'Прямоугольник'

    def __init__(self, a: float, b: float) -> None:
        self.a = a
        self.b = b

    def area(self) -> float:
        return round((self.a * self.b), 2)

    @staticmethod
    def diagonal(a: float, b: float) -> float:
        return round(((a ** 2 + b ** 2) ** (1 / 2)), 2)


class Rhombus(Shape):
    title: str = 'Ромб'

    def __init__(self, a: float, h: float) -> None:
        self.a = a
        self.h = h

    def area(self) -> float:
        return round((self.a * self.h), 2)


class Trapezoid(Shape):
    title: str = 'Трапеция'

    def __init__(self, a: float, b: float, h: float) -> None:
        self.a = a
        self.b = b
        self.h = h

    def area(self) -> float:
        return round((((self.a + self.b) / 2) * self.h), 2)

    @staticmethod
    def middle_line(a: float, b: float) -> float:
        return round(((a + b) / 2), 2)


class Triangle(Shape):
    title: str = 'Треугольник'

    def __init__(self, a: float, b: float, c: float) -> None:
        self.a = a
        self.b = b
        self.c = c

    def area(self) -> float:
        p: float = (self.a + self.b + self.c) / 2
        return round(((p * (p - self.a) * (p - self.b) * (p - self.c)) ** (1/2)), 2)

    @staticmethod
    def middle_line(a: float, b: float, c: float) -> Tuple[float, float, float]:
        middle_line_a: float = a / 2
        middle_line_b: float = b / 2
        middle_line_c: float = c / 2
        return (middle_line_a, middle_line_b, middle_line_c)


class Circle(Shape):
    title: str = 'Круг'

    def __init__(self, r: float) -> None:
        self.r = r

    def area(self) -> float:
        return round((pi * (self.r ** 2)), 2)

    @staticmethod
    def circumference(r: float) -> float:
        return round((2 * pi * r), 2)


class Cube(Square):
    title: str = 'Куб'

    def area(self) -> float:
        return round((6 * (self.a ** 2)), 2)

    def volume(self) -> float:
        return round((self.a ** 3), 2)


class Parallelepiped(Rectangle):
    title: str = 'Параллелепипед'

    def __init__(self, a: float, b: float, c: float) -> None:
        super().__init__(a, b)
        self.c = c

    def area(self) -> float:
        return round((2 * (self.a * self.b + self.b * self.c + self.a * self.c)), 2)

    def volume(self) -> float:
        return round((self.a * self.b * self.c), 2)

    @staticmethod
    def p_diagonal(a: float, b: float, c: float) -> float:
        return (a**2 + b**2 + c**2) ** (1/2)


class Pyramid(Shape):
    title: str = 'Пирамида'

    def __init__(self, a: float, n: float, h: float) -> None:
        self.a = a
        self.n = n
        self.h = h

    def area(self) -> float:
        L: float = (self.h ** 2 + (self.a / (2 * tan(pi / self.n))) ** 2) ** (1/2)
        s_bp: float = ((self.n * self.a) / 2) * L
        s_po: float = (self.n * self.a ** 2) / (4 * tan(pi / self.n))
        return round((s_bp + s_po), 2)

    def volume(self) -> float:
        s_po: float = (self.n * self.a ** 2) / (4 * tan(pi / self.n))
        return round(((s_po * self.h) / 3), 2)


class Sphere(Circle):
    title: str = 'Сфера'

    def area(self) -> float:
        return round((4 * pi * self.r ** 2), 2)

    def volume(self) -> float:
        return round(((4 * pi * self.r ** 3) / 3), 2)


class Cone(Circle):
    title: str = 'Конус'

    def __init__(self, r: float, h: float) -> None:
        super().__init__(r)
        self.h = h

    def area(self) -> float:
        L: float = (self.r ** 2 + self.h ** 2) ** (1/2)
        return round((pi * self.r * (self.r + L)), 2)

    def volume(self) -> float:
        return round(((pi * (self.r ** 2) * self.h) / 3), 2)


class Cylinder(Circle):
    title: str = 'Цилиндр'

    def __init__(self, r: float, h: float) -> None:
        super().__init__(r)
        self.h = h

    def area(self) -> float:
        return round((2 * pi * self.r * (self.h + self.r)), 2)

    def volume(self) -> float:
        return round((pi * self.r ** 2 * self.h), 2)


