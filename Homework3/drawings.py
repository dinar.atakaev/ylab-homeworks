import tkinter as tk
from typing import Tuple, NoReturn, Any, List
import matplotlib.pyplot as plt
import numpy as np
from math import pi
import models

root = tk.Tk()
root.title('Калькулятор фигур')
root.geometry('250x200')
root['bg'] = '#E0FFFF'



def create_new_window(name):
    window = tk.Tk()
    window.title(name)
    window.geometry('400x550')
    window['bg'] = '#E0FFFF'


    def flat_click(shape_name) -> NoReturn:

        second_label['text'] = ''
        third_label['text'] = ''

        first_calc_label['text'] = ''
        second_calc_label['text'] = ''
        third_calc_label['text'] = ''

        lbl_shape_name['text'] = ''
        lbl_shape_name['text'] = shape_name
        lbl_shape_name.grid(row=3, column=1)

        calculate_btn = tk.Button(window, text='Вычислить',
                        command=lambda: click(shape_name),
                        font=('Arial', 12, 'bold'))
        calculate_btn.grid(row=11, column=0)

        second_entry.grid_forget()
        third_entry.grid_forget()
        draw_btn.grid_forget()

        if shape_name == 'Круг':
            first_label['text'] = 'Введите радиус:'
            first_entry.grid(row=8, column=1)

        if shape_name == 'Квадрат':
            first_label['text'] = 'Введите сторону:'
            first_entry.grid(row=8, column=1)

        if shape_name == 'Прямоугольник':
            first_label['text'] = 'Введите сторону a'
            second_label['text'] = 'Введите сторону b'

            first_entry.grid(row=8, column=1)
            second_entry.grid(row=9, column=1)

        if shape_name == 'Треугольник':
            first_label['text'] = 'Введите сторону a'
            second_label['text'] = 'Введите сторону b'
            third_label['text'] = 'Введите сторону c'

            first_entry.grid(row=8, column=1)
            second_entry.grid(row=9, column=1)
            third_entry.grid(row=10, column=1)

        if shape_name == 'Трапеция':
            first_label['text'] = 'Введите сторону a'
            second_label['text'] = 'Введите сторону b'
            third_label['text'] = 'Введите высоту h'

            first_entry.grid(row=8, column=1)
            second_entry.grid(row=9, column=1)
            third_entry.grid(row=10, column=1)

        if shape_name == 'Ромб':
            first_label['text'] = 'Введите сторону a'
            second_label['text'] = 'Введите высоту h'

            first_entry.grid(row=8, column=1)
            second_entry.grid(row=9, column=1)

    def volume_click(shape_name) -> NoReturn:

        calculate_btn = tk.Button(window, text='Вычислить',
                        command=lambda: click(shape_name),
                        font=('Arial', 12, 'bold'))
        calculate_btn.grid(row=11, column=0)

        second_label['text'] = ''
        third_label['text'] = ''
        first_calc_label['text'] = ''
        second_calc_label['text'] = ''
        third_calc_label['text'] = ''
        
        lbl_shape_name['text'] = ''
        lbl_shape_name['text'] = shape_name
        lbl_shape_name.grid(row=3, column=1)

        second_entry.grid_forget()
        third_entry.grid_forget()
        draw_btn.pack_forget()

        if shape_name == 'Куб':
            first_label['text'] = 'Введите сторону a'
            first_entry.grid(row=8, column=1)

        if shape_name == 'Параллелепипед':
            first_label['text'] = 'Введите сторону a'
            second_label['text'] = 'Введите сторону b'
            third_label['text'] = 'Введите сторону c'

            first_entry.grid(row=8, column=1)
            second_entry.grid(row=9, column=1)
            third_entry.grid(row=10, column=1)

        if shape_name == 'Пирамида':
            first_label['text'] = 'Введите сторону a'
            second_label['text'] = 'Количество сторон n'
            third_label['text'] = 'Введите высоту h'

            first_entry.grid(row=8, column=1)
            second_entry.grid(row=9, column=1)
            third_entry.grid(row=10, column=1)

        if shape_name == 'Сфера':
            first_label['text'] = 'Введите радиус r'
            first_entry.grid(row=8, column=1)

        if shape_name == 'Конус':
            first_label['text'] = 'Введите радиус r'
            second_label['text'] = 'Введите высоту h'

            first_entry.grid(row=8, column=1)
            second_entry.grid(row=9, column=1)

        if shape_name == 'Цилиндр':
            first_label['text'] = 'Введите радиус r'
            second_label['text'] = 'Введите высоту h'

            first_entry.grid(row=8, column=1)
            second_entry.grid(row=9, column=1)

    def click(shape_name) -> NoReturn:
        first_entry_data = first_entry.get()
        second_entry_data = second_entry.get()
        third_entry_data = third_entry.get()
        third_calc_label['text'] = ''
        draw_btn.grid(row=13, column=1)

        if shape_name == 'Круг':
            r: float = float(first_entry_data)
            circle: models.Circle = models.Circle(r)
            first_calc_label['text'] = f'Площадь круга: {circle.area()}'
            second_calc_label['text'] = f'Длина круга: {models.Circle.circumference(r)}'

            plot_circle = plt.Circle((0, 0), r, color='b', fill=False)
            ax: Any = plt.gca()
            ax.cla()
            ax.set_xlim((-20, 20))
            ax.set_ylim((-20, 20))
            ax.add_patch(plot_circle)

        if shape_name == 'Квадрат':
            a: float = float(first_entry_data)
            square: models.Square = models.Square(a)
            first_calc_label['text'] = f'Площадь квадрата: {square.area()}'
            second_calc_label['text'] = f'Диагональ квадрата: {models.Square.diagonal(a)}'

            plot_square = plt.Rectangle((0, 0), a, a, color='b', fill=False)
            ax: Any = plt.gca()
            ax.cla()
            ax.set_xlim((-20, 20))
            ax.set_ylim((-20, 20))
            ax.add_patch(plot_square)

        if shape_name == 'Прямоугольник':
            a: float = float(first_entry_data)
            b: float = float(second_entry_data)
            rectangle: models.Rectangle = models.Rectangle(a, b)
            first_calc_label['text'] = f'Площадь прямоугольника: {rectangle.area()}'
            second_calc_label['text'] = f'Диагональ прямоугольника: {models.Rectangle.diagonal(a, b)}'

            plot_rectangle = plt.Rectangle((0, 0), a, b, color='b', fill=False)
            ax: Any = plt.gca()
            ax.cla()
            ax.set_xlim((-20, 20))
            ax.set_ylim((-20, 20))
            ax.add_patch(plot_rectangle)

        if shape_name == 'Треугольник':
            a: float = float(first_entry_data)
            b: float = float(second_entry_data)
            c: float = float(third_entry_data)
            triangle: models.Triangle = models.Triangle(a, b, c)
            first_calc_label['text'] = f'Площадь треугольника: {triangle.area()}'
            second_calc_label['text'] = f'Средние линии: {models.Triangle.middle_line(a, b, c)}'

            x: float = (a ** 2 + b ** 2 - c ** 2) / (2 * a)
            y: float = (b ** 2 - x ** 2) ** (1 / 2)

            points: Tuple[List[float, float], ...] = ([0, 0], [a, 0], [x, y])
            plot_triangle = plt.Polygon(points, color='b', fill=False)
            ax: Any = plt.gca()
            ax.cla()
            ax.set_xlim((-20, 20))
            ax.set_ylim((-20, 20))
            ax.add_patch(plot_triangle)

        if shape_name == 'Трапеция':
            a: float = float(first_entry_data)
            b: float = float(second_entry_data)
            h: float = float(third_entry_data)
            trapezoid: models.Trapezoid = models.Trapezoid(a, b, h)
            first_calc_label['text'] = f'Площадь трапеции: {trapezoid.area()}'
            second_calc_label['text'] = f'Средняя линия: {models.Trapezoid.middle_line(a, b)}'

            points: Tuple[List[float, float], ...] = ([0, 0], [a, 0], [a / 2 + b / 2, h], [a / 2 - b / 2, h])
            plot_trapezoid = plt.Polygon(points, color='b', fill=False)
            ax: Any = plt.gca()
            ax.cla()
            ax.set_xlim((-20, 20))
            ax.set_ylim((-20, 20))
            ax.add_patch(plot_trapezoid)

        if shape_name == 'Ромб':
            a: float = float(first_entry_data)
            h: float = float(second_entry_data)
            rhombus: models.Rhombus = models.Rhombus(a, h)
            first_calc_label['text'] = f'Площадь ромба: {rhombus.area()}'
            second_calc_label['text'] = ''

            points: Tuple[List[float, float], ...] = ([0, 0], [a, 0], [a + (a ** 2 - h ** 2) ** (1 / 2), h],
                                                      [(a ** 2 - h ** 2) ** (1 / 2), h])
            plot_rhombus = plt.Polygon(points, color='b', fill=False)
            ax: Any = plt.gca()
            ax.cla()
            ax.set_xlim((-20, 20))
            ax.set_ylim((-20, 20))
            ax.add_patch(plot_rhombus)

        if shape_name == 'Куб':
            a: int = int(first_entry_data)
            cube: models.Cube = models.Cube(a)
            first_calc_label['text'] = f'Площадь куба: {cube.area()}'
            second_calc_label['text'] = f'Объем куба: {cube.volume()}'

            axes: List[int] = [a, a, a]
            data: Any = np.ones(axes, dtype=np.bool)
            fig: Any = plt.figure()
            ax: Any = fig.add_subplot(111, projection='3d')
            ax.voxels(data, facecolors='r')

        if shape_name == 'Параллелепипед':
            a: int = int(first_entry_data)
            b: int = int(second_entry_data)
            c: int = int(third_entry_data)
            parallelepiped: models.Parallelepiped = models.Parallelepiped(a, b, c)
            first_calc_label['text'] = f'Площадь параллелепипеда: {parallelepiped.area()}'
            second_calc_label['text'] = f'Объем параллелепипеда: {parallelepiped.volume()}'
            third_calc_label['text'] = f'Диагональ параллелепипеда {models.Parallelepiped.p_diagonal(a, b, c)}'

            axes: List[int] = [a, b, c]
            data: Any = np.ones(axes, dtype=np.bool)
            fig: Any = plt.figure()
            ax: Any = fig.add_subplot(111, projection='3d')
            ax.voxels(data, facecolors='r')

        if shape_name == 'Пирамида':
            a: int = int(first_entry_data)
            n: int = int(second_entry_data)
            h: int = int(third_entry_data)
            pyramid: models.Pyramid = models.Pyramid(a, n, h)
            first_calc_label['text'] = f'Площадь пирамиды: {pyramid.area()}'
            second_calc_label['text'] = f'Объем параллелепипеда: {pyramid.volume()}'

            r: float = a / (2 * np.sin(pi / n))
            X: List[float] = []
            Y: List[float] = []
            Z: Any = np.zeros(n + 1)
            Z[n] = h

            for i in range(0, n):
                x: float = r * np.cos((2 * pi * i) / n)
                y: float = r * np.sin((2 * pi * i) / n)
                X.append(x)
                Y.append(y)

            X.append(0)
            Y.append(0)
            fig: Any = plt.figure()
            ax: Any = fig.add_subplot(111, projection='3d')
            ax.plot_trisurf(X, Y, Z, color='b')

        if shape_name == 'Сфера':
            r: float = float(first_entry_data)
            sphere: models.Sphere = models.Sphere(r)
            first_calc_label['text'] = f'Площадь сферы: {sphere.area()}'
            second_calc_label['text'] = f'Объем сферы: {sphere.volume()}'

            u: Any = np.linspace(0, 2 * pi, 100)
            v: Any = np.linspace(0, pi, 100)

            x: float = r * np.outer(np.cos(u), np.sin(v))
            y: float = r * np.outer(np.sin(u), np.sin(v))
            z: float = r * np.outer(np.ones(np.size(u)), np.cos(v))

            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.plot_surface(x, y, z, color='b')

        if shape_name == 'Конус':
            r: float = float(first_entry_data)
            h: float = float(second_entry_data)
            cone: models.Cone = models.Cone(r, h)
            first_calc_label['text'] = f'Площадь конуса: {cone.area()}'
            second_calc_label['text'] = f'Объем конуса: {cone.volume()}'

            theta: Any = np.linspace(0, 2 * pi, 90)
            radius: Any = np.linspace(0, r, 50)
            T, R = np.meshgrid(theta, radius)

            x: float = R * np.cos(T)
            y: float = R * np.sin(T)
            z: float = np.sqrt(x ** 2 + y ** 2) * (h / r)

            fig: Any = plt.figure()
            ax: Any = fig.add_subplot(111, projection='3d')
            ax.plot_surface(x, y, z)

            ax.invert_zaxis()

        if shape_name == 'Цилиндр':
            r: float = float(first_entry_data)
            h: float = float(second_entry_data)
            cylinder: models.Cylinder = models.Cylinder(r, h)
            first_calc_label['text'] = f'Площадь цилиндра: {cylinder.area()}'
            second_calc_label['text'] = f'Объем цилиндра: {cylinder.volume()}'

            u: Any = np.linspace(0, 2 * pi, 50)
            height: Any = np.linspace(0, h, 20)

            x: float = r * np.outer(np.sin(u), np.ones(np.size(height)))
            y: float = r * np.outer(np.cos(u), np.ones(np.size(height)))
            z: float = np.outer(np.ones(np.size(u)), height)

            fig: Any = plt.figure()
            ax: Any = fig.add_subplot(111, projection='3d')
            ax.plot_surface(x, y, z)



    def create_new_flat_btn(shape_name, count):
        tk.Button(window, text=f'{shape_name}', command=lambda: flat_click(shape_name),
                  activebackground='red', font=('Arial', 12, 'bold')).grid(row=count, column=0, sticky='ew')
    def create_new_vol_btn(shape_name, count):
        tk.Button(window, text=f'{shape_name}', command=lambda: volume_click(shape_name),
                  activebackground='red', font=('Arial', 12, 'bold')).grid(row=count, column=0, sticky='ew')
    first_frame = tk.Frame(window)
    second_frame = tk.Frame(window)
    third_frame = tk.Frame(window)

    lbl_shape_name = tk.Label(window, font=('Arial', 16, 'bold'), justify='center',
                   bg='#E0FFFF', fg="#8B0000")

    first_label = tk.Label(first_frame, text='', font=('Arial', 12), justify='left', bg='#E0FFFF')
    first_entry = tk.Entry(first_frame, width=20)

    second_label = tk.Label(second_frame, text='', font=('Arial', 12), justify='left', bg='#E0FFFF')
    second_entry = tk.Entry(second_frame, width=20)

    third_label = tk.Label(third_frame, text='', font=('Arial', 12), justify='left', bg='#E0FFFF')
    third_entry = tk.Entry(third_frame, width=20)

    first_calc_label = tk.Label(window, text='', font=('Arial', 12), justify='left', bg='#E0FFFF')
    second_calc_label = tk.Label(window, text='', font=('Arial', 12), justify='left', bg='#E0FFFF')
    third_calc_label = tk.Label(window, text='', font=('Arial', 12), justify='left', bg='#E0FFFF')

    first_label.grid(row=8, column=0)
    second_label.grid(row=9, column=0)
    third_label.grid(row=10, column=0)

    first_frame.grid(row=8, column=1)
    second_frame.grid(row=9, column=1)
    third_frame.grid(row=10, column=1)

    draw_btn = tk.Button(window, text='Показать фигуру', command=lambda: plt.show(), font=('Arial', 12, 'bold'))

    first_calc_label.grid(row=11, column=1)
    second_calc_label.grid(row=12, column=1)
    third_calc_label.grid(row=13, column=1)





    flat = [models.Circle.title, models.Square.title, models.Rectangle.title,
            models.Triangle.title, models.Trapezoid.title, models.Rhombus.title]
    vol = [models.Cube.title, models.Parallelepiped.title, models.Pyramid.title,
           models.Sphere.title, models.Cone.title, models.Cylinder.title]


    count = 0
    if name == "Плоские фигуры":
        for x in flat:
            count += 1
            create_new_flat_btn(x, count)

    elif name == "Объемные фигуры":
        for x in vol:
            count += 1
            create_new_vol_btn(x, count)




lbl1 = tk.Label(text='Выберите фигуру', font=('Arial', 16, 'bold'), height=2, bg='#E0FFFF')
btn_flat = tk.Button(text='Плоские фигуры', command=lambda: create_new_window('Плоские фигуры'),
                     font=('Arial', 16), height=2)
btn_vol = tk.Button(text='Объемные фигуры', command=lambda: create_new_window('Объемные фигуры'),
                    font=('Arial', 16), height=2)

lbl1.grid(row=0, column=0, sticky='we', ipadx=25)
btn_flat.grid(row=1, column=0, sticky='we', ipadx=25)
btn_vol.grid(row=2, column=0, sticky='we', ipadx=25)
root.mainloop()